from flask import Flask, render_template, Response
import cv2

app = Flask(__name__)
camera = cv2.VideoCapture("http://127.0.0.1:5000/video_feed.mjpg")


def gen_frames():  
    while True:
        success, frame = camera.read()
        if not success:
            print("Error")
            break
        else:
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            cv2.putText(frame, "text", (100, 100), cv2.FONT_HERSHEY_SIMPLEX,
				0.5, color, 2)
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed.mjpg')
def video_feed():
    return Response(gen_frames(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5001)