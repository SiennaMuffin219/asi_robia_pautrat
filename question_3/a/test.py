import requests
import cv2

addr = 'http://127.0.0.1:5001/detect_image'

# prepare headers for http request
content_type = 'image/jpeg'
headers = {'content-type': content_type}

img = cv2.imread('voitures.png')
# encode image as jpeg
_, img_encoded = cv2.imencode('.jpg', img)
# send http request with image and receive response
response = requests.post(addr, data=img_encoded.tostring(), headers=headers)
# decode response
print(response)